FROM ubuntu:16.04

MAINTAINER Edgard Pineda <edgard.pineda@gmail.com>

RUN apt-get clean && apt-get -y update && apt-get install -y locales && locale-gen en_US.UTF-8

ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LC_CTYPE=UTF-8
ENV LANG=en_US.UTF-8
ENV TERM xterm

RUN apt-get dist-upgrade -y --force-yes && apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:git-core/ppa && apt-get update

RUN apt-get install -y --force-yes \
  less \
  vim \
	git \
	python \
	python-dev \
	python-setuptools \
	virtualenv \
	nginx \
	supervisor \
	sqlite3 \
	mysql-client \
	postgresql-client \
	gcc \
  libgeoip1 \
  graphviz \
	libssl-dev \
	libldap2-dev \
	libsasl2-dev	\
	libmysqlclient-dev \
	libpq-dev \
  && apt-get clean

RUN apt-get install -y rsyslog \
  && apt-get clean

RUN \
  echo "postfix postfix/main_mailer_type string 'Internet Site'\npostfix postfix/mailname string mail.example.com" | debconf-set-selections \
  && apt-get install -y postfix \
  && apt-get clean

RUN rm -rf /var/lib/apt/lists/*

RUN cp -f /etc/services /var/spool/postfix/etc/
RUN cp -f /etc/resolv.conf /var/spool/postfix/etc/

RUN easy_install pip

RUN apt-get update && apt-get dist-upgrade -y --force-yes

RUN apt-get install -y --force-yes libgraphviz-dev pkg-config

RUN mkdir /var/log/celery

RUN mkdir /var/run/celery

COPY supervisor-app.conf /etc/supervisor/conf.d/

ARG DJANGO_PROJECT_NAME

ENV DJANGO_PROJECT_NAME=${DJANGO_PROJECT_NAME}

CMD ["supervisord", "-n"]
