
# DjangoDock

DjangoDock helps you to run your Django project on Docker very quickly.

This tool was created inspired on [LaraDock](https://github.com/LaraDock/laradock) and [dockerizing-django](https://github.com/realpython/dockerizing-django).

## Requirements

- git
- docker
- docker-compose
- docker-machine

In your Django project you need to have the requirements.txt file.

## Installation

If you already have a Django project, clone this repository on your project root directory:

```
git submodule add https://gitlab.com/Vigigo/djangodock
```

or clone this repo anywhere you want:

```
git clone add https://gitlab.com/Vigigo/djangodock
```

## Usage

You must define the environment variable "DJANGO_PROJECT_NAME" to the name of your django project.

Running uWSGI-Nginx and MySQL:

```
cd djangodock
DJANGO_PROJECT_NAME=my_django_project_name docker-compose up -d uwsgi-nginx mysql
```

or with Postgres:

```
DJANGO_PROJECT_NAME=my_django_project_name docker-compose up -d uwsgi-nginx postgres
```

If you want to use celery in your django project, start yours dockers instance running:
```
DJANGO_PROJECT_NAME=my_django_project_name docker-compose up -d uwsgi-nginx mysql celery
```

Note: `workspace` will run automatically, so no need to specify it in the up command.

After that you can enter to the workspace container:
```
docker exec -it djangodock_workspace_1 /bin/bash
```
To find the containers names, type:

```
docker-compose ps
```

To view the logs:
```
docker exec -it djangodock_uwsgi-nginx_1 /bin/bash
tail -f /var/log/uwsgi/django.log /var/log/nginx/*log /var/log/supervisor/supervisord.log
```


To access to your Django project, first you need to know yout docker IP address, runnig for example:

```
docker-machine ls

```

Then open your browser and visit your application: `http://x.x.x.x`

To view all docker containers:
```
docker ps -a
```

To stop the containers, type:
```
docker-compose stop
```

## Customization

You can edit the `docker-compose.yml` and all `Dockerfile` files. Change anything you want.
You can customize uWSGI editing `uwsgi_params` and `uwsgi.base.ini`.

Check the entire repository to find another customizable files.

## License

[MIT License](LICENSE) (MIT)
